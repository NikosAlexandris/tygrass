# GRASS GIS in Terminology

Working with [GRASS GIS][] and the [Terminology][] terminal,
on the command line under Linux OSes, may offer a better experience.
GRASS GIS can benefit from Terminology's capacity to display images.

>The excercises documented hereafter assume familiarity with bash, Python and
scripting for GRASS GIS

Watch some demo [screencasts][]:

<iframe src="https://archive.org/embed/GrassGisUnderTerminology" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

[screencasts]:
https://archive.org/details/GrassGisUnderTerminology "GRASS GIS under Terminology [Screencast]"

## The tools

**<span style="color:#008A28">GRASS GIS</span>** is a geographic information
system (GIS) software that can handle raster, topological vector and graphic
data.

**Terminology** is a terminal emulator for Linux/BSD/UNIX systems that uses
EFL. Among the many features and the support for 256 colors, it can also
display image files in all their alpha channel splendor (SVG, PDF and PS files
will display and scale correctly).

[GRASS GIS]: https://grass.osgeo.org/ "Geographic Resources Analysis Support System"
[Terminology]: https://www.enlightenment.org/about-terminology "Terminology"

## Images in Terminology

Get one of the official <span style="color:#008A28">GRASS
GIS</span> logos and render it inside Terminology:

```bash
wget https://grass.osgeo.org/uploads/images/logo/grassgis_logo_colorlogo_text_alphabg.png
tycat grassgis_logo_colorlogo_text_alphabg.png
```
<!-- ![GRASS GIS logo](grassgis_logo_colorlogo_text_alphabg.png "GRASS GIS logo") -->
<img src="grassgis_logo_colorlogo_text_alphabg.png" alt="GRASS GIS logo" width="250"/>

Here's a screenshot:

![Screenshot displaying the GRASS GIS logo inside Terminology](screenshot-2018-10-08-193136.png "Title")

This proves to be particularly useful when scripting for GRASS GIS. With a few
additions in GRASS GIS' `bashrc` file, terminology's commands `tycat` (as well
as `tyls`) can be used to display GRASS GIS raster maps among other output of a
running script.

`tycat` offers some options such as scaling the image:
```bash
tycat -g 30x30 grassgis_logo_colorlogo_text_alphabg.png
```
![Screenshot displaying the GRASS GIS logo inside Terminology](screenshot-2018-10-08-193146.png "Title")

## Rendering in GRASS GIS

GRASS GIS' command line renders directly into a file. The available
[GRASS GIS display drivers](https://grass.osgeo.org/grass74/manuals/displaydrivers.html "display drivers") are currently: `Cairo`, `PNG`, `PS` (Postscript) and `HTMLMAP`.
The driver can be selected by setting the `GRASS_RENDER_IMMEDIATE` variable or
by running the `d.mon` module.
Setting the `GRASS_RENDER_FILE` to any file name with the extension `.png`,
enables drawing content to it using GRASS GIS' any `d.*` command (see also a
short [screencast][screencast 2])

[screencast 2]:
https://archive.org/details/GrassGisUnderTerminology/02_grassgis_command_line_rendering.mp4 "GRASS GIS' command line rendering mechanism [Screencast]"

Let's try so!

> (**To Do**: Provide sample data used in this documentation or repeat with
some of [GRASS GIS' sample data][sample data])

[sample data]: https://grass.osgeo.org/download/sample-data/ "GRASS GIS sample data collection"

Inside a **<span style="color:#008A28">GRASS GIS</span>** session ([screencast][screencast 1]), there is a raster map named
`corine_land_cover_2006`. See:

[screencast 1]:
https://archive.org/details/GrassGisUnderTerminology/01_terminology.mp4 "GRASS GIS session inside Terminology [Screencast]"

```bash
r.info  -g
```
returns
```bash
north=2879700
south=2748850
east=4854650
west=4735600
nsres=99.9618029029794
ewres=99.9580184718724
rows=1309
cols=1191
cells=1559019
datatype=CELL
ncats=50
```

First, ensure the computational region is correctly set
```bash
g.region raster=corine_land_cover_2006 -p
projection: 99 (ETRS89 / LAEA Europe)
zone:       0
datum:      etrs89
ellipsoid:  grs80
north:      2879700
south:      2748850
west:       4735600
east:       4854650
nsres:      99.9618029
ewres:      99.95801847
rows:       1309
cols:       1191
cells:      1559019
```

Then, settting the output file name via
```bash
export GRASS_RENDER_FILE=/home/nik/grass_render_file.png
```
and drawing the map via
```bash
d.rast corine_land_cover_2006
```

Displaying the image `/home/nik/grass_render_file.png` in Terminology using the
`tycat` utility ([screencast][screencast 3]).

[screencast 3]:
https://archive.org/details/GrassGisUnderTerminology/03_tycat_examples.mp4 "`tycat` examples [Screencast]"

```bash
tycat /home/nik/grass_render_file.png
```
![tycat](screenshot-2018-10-11-140826.png "tycat /home/nik/grass_render_file.png ")


## Bash functions

Based on the above, we write short bash functions to use from inside a GRASS
GIS session. These may be written as part of `grass.bashrc` as detailed below.

Of course, first we need to set some required environment variables for GRASS
GIS.

```bash
# display library driver: cairo, png, ps, html or default
export GRASS_RENDER_IMMEDIATE=cairo
export GRASS_RENDER_TRUECOLOR=TRUE
export GRASS_RENDER_TRANSPARENT=TRUE

# png driver
export GRASS_PNG_AUTO_WRITE=TRUE
GRASS_RENDER_PATH=/geo/grassdb/render
GRASS_RENDER_FILENAME=grass_render_file.png
export GRASS_RENDER_FILE=${GRASS_RENDER_PATH}/${GRASS_RENDER_FILENAME}
echo "Render output file:                      $GRASS_RENDER_FILE"
echo
export GRASS_RENDER_FILE_READ=TRUE
export GRASS_PNGFILE_NAME=grass_pngfile.png # for grass7
export GRASS_PNGFILE=${GRASS_RENDER_PATH}/${GRASS_PNGFILE_NAME} # for grass7
export GRASS_PNG_COMPRESSION=9
```

Then, a simple function to display raster maps on the command line:
```bash
function tygrass {
    tycat $GRASS_RENDER_FILE
}
```
and here an example:

```bash
d.erase
d.rast population_2015
tygrass
```

![tygrass.ls](screenshot-2018-10-13-010522.png "tygrass.ls")

See also these demo screencasts on using `tygrass`:
[`tygrass` example 1][screencast 4],
[`tygrass` example 2][screencast 5],
[`tygrass` example 3][screencast 6].

[screencast 4]:
https://archive.org/details/GrassGisUnderTerminology/04_tygrass_example.mp4 "Example of using `tygrass` 1 [Screencast]"
[screencast 5]:
https://archive.org/details/GrassGisUnderTerminology/05_tygrass_another_example.mp4 "Example of using `tygrass` 2 [Screencast]"
[screencast 6]:
https://archive.org/details/GrassGisUnderTerminology/06_tygrass_yet_another_example.mp4 "Example of using `tygrass` 3 [Screencast]"

#### Raster maps

Instead of rendering the content of `GRASS_RENDER_FILE`, we can do better and
save the images with the actual map name. This is useful when documenting
processes as images are easier to identify and copy to wherever is required.

The following funtion will modify momentarily the variable `GRASS_RENDER_FILE`
and reset it back to what it was (actually, to what the user wants it to be)
once complete.

```bash
function tyraster {
    # set output filename and draw the raster map
    export GRASS_RENDER_FILE="${GRASS_RENDER_PATH}/$1".png

    # d.erase bgcolor=black
    d.erase bgcolor=white

    d.rast "$@"
    # Break in error here! FIXME
    echo "$GRASS_RENDER_FILE"
    tycat -g 300x300 "$GRASS_RENDER_FILE"

    # reset filename
    export GRASS_RENDER_FILE="${GRASS_RENDER_PATH}/${GRASS_RENDER_FILENAME}"
}
```

Do
```bash
tyraster area_of_interest
```
and get

![tyraster](screenshot-2018-10-13-012545.png "tyraster")

See these screencasts on using `tyraster`:
[`tyraster` example 1][screencast 7],
[`tyraster` example 2][screencast 8].

[screencast 7]:
https://archive.org/details/GrassGisUnderTerminology/07_tyraster_example.mp4 "Example of using `tyraster` [Screencast]"
[screencast 8]:
https://archive.org/details/GrassGisUnderTerminology/08_tyraster_another_example.mp4 "Example of using `tyraster` [Screencast]"

###### One caveat
> GRASS GIS modules feature named input and output options, thus the
order doesn't matter. In this simple bash function, the order matters: the
raster map name must be provided first! The rest of options, then, is passed to
the `d.rast` command and their order does not matter.

#### Histogram of a raster map

The following function will plot the histogram of a raster map and store it
wherever `$GRASS_RENDER_FILE` points to.

```bash
function tygrass.histogram {

    # draw the histogram
    export GRASS_RENDER_FILE="${GRASS_RENDER_PATH}/${1}.histogram".png
    d.erase bgcolor=white
    d.histogram "$@"

    # Break in error here! FIXME

    echo "$GRASS_RENDER_FILE"
    tycat -g 300x300 "$GRASS_RENDER_FILE"
    # reset back!
    export GRASS_RENDER_FILE="${GRASS_RENDER_PATH}/${GRASS_RENDER_FILENAME}"
}
```

#### Vector maps

Likewise, we can do for rendering vector maps:
```bash
function tyvector {
    export GRASS_RENDER_FILE=/geo/grassdb/render/$1.png
    d.erase bgcolor=black
    d.vect "$@"
    echo "$GRASS_RENDER_FILE"
    tycat -g 300x300 "$GRASS_RENDER_FILE"
    export GRASS_RENDER_FILE=/geo/grassdb/render/grass_render_file.png
}
```

A somewhat elaborated example:
```bash
tyvector regions fcolor=none color=indigo display=shape,cat
```

![tyvector](screenshot-2018-10-13-013918.png "tyvector")

<strong>Idea:</strong>
<em>Both</em> `tyraster`
<em>and</em>
`tyvector`
<em>would deserve some additions, like setting the width and height of the
image to draw.</em>

See this related [screencast][screencast 9] on using `tyvector`.

[screencast 9]:
https://archive.org/details/GrassGisUnderTerminology/09_tyvector_example.mp4 "Example of using `tyraster` [Screencast]"

###### Caveat

> Likewise to the `tyraster` function, the vector map name must be provided
first! The rest of options, then, is passed to the `d.vect` command and their
order does not matter.

#### Keep tidy

Using `tyraster` and `tyvector`, creates in time more and more PNG files. Here
is a function to clean up all PNG files manually:
```bash
function ty.clean.grass {
    find $GRASS_RENDER_PATH/ -type f \( -iname \*.png ! -name grass_render_file.png \) -exec mv {} /tmp \;
}
```

A function to list all PNG files that reside inside `GRASS_RENDER_PATH`:
```bash
function tygrass.ls {
    tyls -m $GRASS_RENDER_PATH
}
```
![tygrass.ls](screenshot-2018-10-08-195523.png "tygrass.ls")

See this related [screencast][screencast 10] on using `ty.grass.ls` and
`ty.clean.grass`.

[screencast 10]:
https://archive.org/details/GrassGisUnderTerminology/10_tygrass.ls_and_ty.clean.grass_examples.mp4 "Examples using `tygrass.ls` and `ty.clean.grass` [Screencast]"


## Python scripting

In a GRASS GIS script, first comes the description of the interface. There,
define for example a `d` flag:
```python
#%flag
#%  key: d
#%  description: Draw maps in terminology (developper's version)
#%end
```

Then, import required librairies, such as:
```python
import grass.script as grass
from grass.exceptions import CalledModuleError
from grass.pygrass.modules.shortcuts import general as g
from grass.pygrass.modules.shortcuts import raster as r
from grass.pygrass.modules.shortcuts import vector as v
```

Using `tycat`, a helper function as the following can be built to draw maps on
the terminal, while executing a GRASS GIS script.

```python
def draw_map(mapname, **kwargs):
    """
    Set the GRASS_RENDER_FILE and draw the requested raster map using
    Terminology's `tycat` utility.

    Parameters
    ----------
    mapname :
        Name of input raster map to draw on terminology

    width :
        Optional parameter to set the width

    height :
        Optional parameter to set the height

    Returns
    -------
    This function does not return any object

    Examples
    --------
    ..
    draw_map(raster_map_name)
    draw_map(raster_map_name, width='50', height='50')
    ..
    """
    if flags['d']:

        # Local globals
        width = 30
        height = 30

        # g.message(_("Map name: {m}".format(m=mapname)))
        # Do not draw maps that are all 0
        minimum = grass.raster_info(mapname)['min']
        grass.debug(_("Minimum: {m}".format(m=minimum)))
        # g.message(_("Minimum: {m}".format(m=minimum)))
        maximum = grass.raster_info(mapname)['max']
        grass.debug(_("Maximum: {m}".format(m=maximum)))
        # g.message(_("Maximum: {m}".format(m=maximum)))

        if minimum == None and maximum == None:
            msg = "All cells in map '{name}' are NULL. "
            msg += "If unexpected, please check for processing errors."
            msg = msg.format(name=mapname)
            grass.warning(_(msg))
            return

        if minimum == 0 and maximum == 0:
            msg = "Map '{name}' is all 0. Not drawing".format(name=mapname)
            grass.warning(_(msg))
            return

        render_file = "{directory}/{prefix}{name}{suffix}.{extension}"
        if 'prefix' in kwargs:
            prefix = kwargs.get('prefix') + '_'
        else:
            prefix = ''

        if 'suffix' in kwargs:
            suffix = '_' + kwargs.get('suffix')
        else:
            suffix = ''

        render_file = render_file.format(
                directory = grass_render_directory,
                prefix=prefix,
                name = mapname,
                suffix=suffix,
                extension = 'png')

        os.putenv("GRASS_RENDER_FILE", render_file)

        run("d.erase", bgcolor='black', flags='f')
        run("d.rast", map=mapname)
        # run("d.rast.leg", map=mapname)

        if 'width' in kwargs and 'height' in kwargs:
            width = kwargs.get('width')
            height = kwargs.get('height')

        g.message(_(" >>> Map file: {f}".format(f=render_file)))
        command = "tycat -g {w}x{h} {filename}"
        command = command.format(filename=render_file, w=width, h=height)
        subprocess.Popen(command.split())
```

Now it's just a short call of `draw_map()` to display any raster map at any
point of a Python script.  This is useful while developing and debugging a
script to follow up on how a resulting raster map looks like. See how this
looks like in this [screencast][screencast 11].

[screencast 11]:
https://archive.org/details/GrassGisUnderTerminology/11_grassgis_running_a_script_with_a_custom_d_flag.mp4 "Running a GRASS GIS script with a special `-d` flag [Screencast]"

#### Experimenting

Going further, here an experimental addon: https://gitlab.com/NikosAlexandris/d.tyraster
